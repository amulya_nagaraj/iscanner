package android.primary.com.iscanner.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.primary.com.iscanner.R;

public class OnboardingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
    }
}
