/*
 * Copyright 2016 Y Media Labs. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package android.primary.com.iscanner.network;

import android.primary.com.iscanner.models.SendCodeRequest;
import android.primary.com.iscanner.models.StringResponse;
import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by manjunath on 9/30/16.
 */

public interface AwbApis {


    @NonNull
    @POST("/api/user-products/return/")
    Call<StringResponse> sendProductId(@Body SendCodeRequest sendCodeRequest);
}
