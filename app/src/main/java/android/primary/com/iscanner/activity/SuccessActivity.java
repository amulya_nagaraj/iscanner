package android.primary.com.iscanner.activity;

import android.content.Intent;
import android.primary.com.iscanner.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import static android.primary.com.iscanner.R.id.txt_title;

public class SuccessActivity extends AppCompatActivity {

    private TextView txtSuccessMsg;
    private RelativeLayout backRl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setData();
    }

    private void setData() {
        if (getIntent() != null) {
            txtSuccessMsg.setText(getIntent().getStringExtra(ApiConstants.Bundle.SUCCESS_MSG));
        }
    }

    private void initView() {
        txtSuccessMsg = (TextView) findViewById(R.id.txt_title);
        backRl = (RelativeLayout) findViewById(R.id.success_bg);
        backRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SuccessActivity.this, SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
