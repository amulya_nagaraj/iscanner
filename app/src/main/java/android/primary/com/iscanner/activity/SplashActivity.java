package android.primary.com.iscanner.activity;

import android.content.Intent;
import android.os.Bundle;
import android.primary.com.iscanner.R;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    private TextView txtScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initView();
    }

    private void initView() {
        txtScan = (TextView) findViewById(R.id.txt_scan);
        txtScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SplashActivity.this, ScanActivity.class);
                startActivity(intent);
            }
        });
    }
}
