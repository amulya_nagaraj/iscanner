package android.primary.com.iscanner;

import android.app.Application;
import android.content.Context;

/**
 * Created by amulya on 1/7/17.
 */

public class ICycleApplication extends Application {
    private static Context mAppContext;

    public static Context getAppContext() {
        return mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();
    }
}
