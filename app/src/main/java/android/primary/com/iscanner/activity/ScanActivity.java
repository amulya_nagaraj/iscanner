package android.primary.com.iscanner.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.primary.com.iscanner.R;
import android.primary.com.iscanner.UiUtils;
import android.primary.com.iscanner.models.ApiErrorResponse;
import android.primary.com.iscanner.models.SendCodeRequest;
import android.primary.com.iscanner.models.StringResponse;
import android.primary.com.iscanner.network.AwbApis;
import android.primary.com.iscanner.network.AwbService;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {

    public static final String TAG = ScanActivity.class.getSimpleName();
    private static final int REQUEST_CAMERA_PERMISSION = 101;
    private ZBarScannerView mScannerView;
    private boolean isFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MarshmallowPermissionManager.checkCameraPermission(this)) {
            mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
            setContentView(mScannerView);
            mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
            mScannerView.startCamera();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.stopCamera();
        }// Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {
        // Do something with the result here
        Log.v(TAG, result.getContents()); // Prints scan results
        Log.v(TAG, result.getBarcodeFormat().getName()); // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
//        mScannerView.resumeCameraPreview(this);
        handleScanCode(result.getContents());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mScannerView = new ZBarScannerView(this);    // Programmatically initialize the scanner view
                    setContentView(mScannerView);
                    mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
                    mScannerView.startCamera();
                } else {
                    finish();
                }
            }
            break;
        }
    }

    private void handleScanCode(String code) {
        if (!isFirst) {
            isFirst = true;
            if (!TextUtils.isEmpty(code)) {
                SendCodeRequest sendCodeReq = new SendCodeRequest();
                sendCodeReq.setProductId(code);
                sendProductCode(sendCodeReq);
            } else {
                Toast.makeText(ScanActivity.this, "Invalid code", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendProductCode(SendCodeRequest sendCodeReq) {
        UiUtils.showProgress(this);
        AwbApis apiService = AwbService.getClient().create(AwbApis.class);
        Call<StringResponse> call =
                apiService.sendProductId(sendCodeReq);
        call.enqueue(new Callback<StringResponse>() {
            @Override
            public void onResponse(Call<StringResponse> call, @NonNull Response<StringResponse> response) {
                UiUtils.hideProgress();
                if (response.errorBody() == null && response.code() == ApiConstants.SUCCESS_200) {
                    Intent intent = new Intent(ScanActivity.this, SuccessActivity.class);
                    intent.putExtra(ApiConstants.Bundle.SUCCESS_MSG, response.body().getDetail());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else if (response.errorBody() != null && response.code() == ApiConstants.ERROR_400) {
                    try {
                        ApiErrorResponse errorResponse = new Gson().fromJson(response.errorBody().string(), ApiErrorResponse.class);
                        Toast.makeText(ScanActivity.this, errorResponse.getDetail(), Toast.LENGTH_SHORT).show();
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else if(response.code() == ApiConstants.ERROR_500){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<StringResponse> call, @NonNull Throwable t) {
                UiUtils.hideProgress();
                Toast.makeText(ScanActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
